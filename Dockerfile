FROM python:3.11-slim

WORKDIR /src

# Install curl
ENV DEBIAN_FRONTEND=non-interactive
RUN apt-get update -q -y && apt-get install -q -y --no-install-recommends curl

# Install worker as a package
COPY worker_demo worker_demo
COPY pyproject.toml ./
RUN pip install . --no-cache-dir

# Add archi local CA
RUN curl https://assets.teklia.com/teklia_dev_ca.pem > /usr/local/share/ca-certificates/arkindex-dev.crt && update-ca-certificates
ENV REQUESTS_CA_BUNDLE /etc/ssl/certs/ca-certificates.crt

CMD ["worker-demo"]
