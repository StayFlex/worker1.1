
# Demo

Demo ML worker for Arkindex

## Development

For development and tests purpose it may be useful to install the worker as a editable package with pip.

```shell
pip3 install -e .
```

## Linter

Code syntax is analyzed before submitting the code.\
To run the linter tools suite you may use pre-commit.

```shell
pip install pre-commit
pre-commit run -a
```

## Run tests

Tests are executed with tox using [pytest](https://pytest.org).

```shell
pip install tox
tox
```

To recreate tox virtual environment (e.g. a dependencies update), you may run `tox -r`
=======
# worker1.1

# Some example

Code to use Yolo Model in a worker: [main](https://scm.univ-tours.fr/22107358t/woyolo2/-/blob/main/worker_woyolo/worker.py?ref_type=heads)

Code to use a Yolo Model trained to recongnize Lettrine in a worker (Made by Thierry Brouard): [main](https://scm.univ-tours.fr/22107358t/woyolo2/-/blob/YoLettrine/worker_woyolo/worker.py?ref_type=heads)

Code to exploit parametter in the worker : [secret](https://scm.univ-tours.fr/22107358t/woyolo2/-/blob/secret/worker_woyolo/worker.py)
