Demo ML worker for Arkindex

#### Configuration

##### Elements

<!-- Describe the elements that should be passed as input to this worker. -->

The **Demo** worker processes *folder|page|word|...* elements.

##### Parameters

The parameters of the configuration used by this worker are described in the table below. To learn more about the different types of parameters, head to the [workers documentation](https://workers.arkindex.org/contents/workers/yaml/#setting-up-user-configurable-parameters).

<!-- Port the content of the section in `.arkindex.yml`. You can add more details about the parameters in the 'Comment' column or below. -->

| Description | Type | Required | Comment |
| ----------- | ---- | :------: | ------- |
| xxxx        | xxxx |   ☒/☐    | xxxx    |

#### Workflow

<!-- Describe what the worker does with the element, in what order, to what end. -->

#### Results

<!-- Describe what the worker produces on Arkindex. It may be ML results (transcriptions, elements, entities, classifications, ...), artifacts... -->

#### Additional resources

<!-- Any external links, e.g. - [Description of website](https://www.url.com/) -->
